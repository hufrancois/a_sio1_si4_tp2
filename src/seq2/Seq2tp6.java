package seq2;

import java.util.Scanner;

public class Seq2tp6 {
    
    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        
        float taille, pdL, pdC = 0;
        int age, morphologie;
        String sexe;
        
        System.out.println("Quelle est votre taille en cm?");
        taille = clavier.nextFloat();
        
        System.out.println("\nQuelle est votre age?");
        age = clavier.nextInt();
        
        System.out.println("\nQuelle est votre sexe (F ou M)?");
        sexe = clavier.next();
        
        System.out.println("\nQuelle est votre morphologie (1 = fine; 2 = normale; 3 = large)?");
        morphologie = clavier.nextInt();
        
        if(sexe.equals("F")) {
            pdL = (float) (taille - 100 - (taille-150) / 2.5);
        }
        else {
            pdL = (float) (taille - 100 - (taille-150) / 4);
        }
        
        if(morphologie == 1) {
            pdC = (float) ((taille-100+age/10)*0.9);
        }
        else if(morphologie == 2) {
            pdC = (float) ((taille-100+age/10)*0.9*0.9);
        }
        else if(morphologie == 3) {
            pdC = (float) ((taille-100+age/10)*0.9*1.1);
        }
        
        System.out.println("\nVotre poids idéal selon Lorentz est de: " + pdL);
        
        System.out.println("Votre poids idéal selon Creff est de: " + pdC);
    }
}
