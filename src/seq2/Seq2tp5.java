package seq2;

import java.util.Scanner;

public class Seq2tp5 {

    public static void main(String[] args) {
         Scanner clavier = new Scanner(System.in);
        float poids, taille, imc;
        String mention; 
        System.out.println("Veuillez saisir votre poids en kg");
        poids = clavier.nextFloat();
        System.out.println("Veuillez saisir votre taille en cm");
        taille = clavier.nextFloat();
        imc = (10000 * poids) / (taille * taille);
        
        if(imc < 19) { 
            mention = "maigreur";
        }
        else if(imc >= 19 && imc < 25) {
            mention = "normal";
        }
        else if(imc >= 25 && imc < 30) {
            mention = "surpoids";
        }
        else {
            mention = "obesite";
        }
        System.out.println("Votre IMC a pour mention: " + mention);
    }
}
