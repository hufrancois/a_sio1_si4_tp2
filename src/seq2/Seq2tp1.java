package seq2;

import java.util.Scanner;

public class Seq2tp1 {

    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        
        float longueur, largeur, perimetre, surface;
        
        System.out.println("Quelle est la longueur du rectangle en cm?");
        longueur = clavier.nextFloat();
        
        System.out.println("\nQuelle est la largeur du rectangle en cm?");
        largeur = clavier.nextFloat();
        
        perimetre = longueur*2+largeur*2;
        surface = longueur*largeur;
        
        System.out.println("\nLe rectangle a un perimetre de " + perimetre + "cm et une surface de " + surface + "cm².");
    }
}
